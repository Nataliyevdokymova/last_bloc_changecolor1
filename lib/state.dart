class StateN {
  StateN();

  factory StateN.changeColor(bool isRed) = ColorProjectStateRed;

  factory StateN.changeColor1(bool isCyan) = ColorProjectStateCyan;
}

class ColorProjectStateRed extends StateN {
  final bool isRed;

  ColorProjectStateRed(this.isRed);}


  class ColorProjectStateCyan extends StateN {
  final bool isCyan;

  ColorProjectStateCyan(this.isCyan);}





