import 'package:flutter/material.dart';
import 'package:last_bloc_changecolor1/bloc.dart';
import 'package:last_bloc_changecolor1/state.dart';



class Project extends StatefulWidget {   ///так описуємо StatefulWidget
  const Project({Key? key}) : super(key: key);    ///так описуємо StatefulWidget

  @override
  State<Project> createState() => _ProjectState();  ///так описуємо StatefulWidget і створюємо состояние
}

class _ProjectState extends State<Project> {    /// створюємо состояние

  late Bloc bloc;   /// створюємо перемінну  bloc  класа Bloc (який створили раніше в файлі Bloc) наводимо на неї і нажимаємо щоб вискочили автоматичні методи і правимо їх

  @override
  void initState() {             /// метод народження перемінної bloc
    bloc = Bloc();
    super.initState();
  }

  @override                       /// метод закінчення роботи перемінної bloc
  void dispose() {
    bloc.dispose();
    super.dispose();
  }


  @override
  Widget build(BuildContext context) {    /// малюємо віджети
    return Scaffold(
      appBar: AppBar(),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            StreamBuilder(                   /// визначаємо частину коду яку буде перемальовувати Bloc за допомогою StreamBuilder
                stream: bloc.getStreamController1,     /// ПЕРЕДАЄМО EVENT БЛОКУ, заставляємо працювати StreamBuilder за допомогою моркрвки (StreamController),повязуємо з блоком.  в переменной bloc. викзиваємо метод getStreamController1
                builder: (BuildContext context, snapshot) {          /// snapshot це і є наш state

                  print('11111 $snapshot');
                  if(snapshot.data is ColorProjectStateRed){
                    print('222222');
                    ColorProjectStateRed state = snapshot.data as ColorProjectStateRed;    /// якщо ColorProjectState state дорівнює состоянию (snapshot.data) ColorProjectState   то ...
                    return Container(
                      width: 100,
                      height: 100,
                      color: state.isRed ? Colors.green : Colors.yellowAccent,
                    );
                  }

                  return Container(        /// визначаємо вигляд віджета по замовчуванню до включення Bloc
                    width: 100,
                    height: 100,
                    color: Colors.deepPurple,
                  );
                }),
            SizedBox(height: 15.0),

            StreamBuilder(                   /// визначаємо частину коду яку буде перемальовувати Bloc за допомогою StreamBuilder
                stream: bloc.getStreamController2,     /// ПЕРЕДАЄМО EVENT БЛОКУ, заставляємо працювати StreamBuilder за допомогою моркрвки (StreamController),повязуємо з блоком.  в переменной bloc. викзиваємо метод getStreamController1
                builder: (BuildContext context, snapshot) {          /// snapshot це і є наш state

                  print('11111 $snapshot');
                  if(snapshot.data is ColorProjectStateCyan){
                    print('222222');
                    ColorProjectStateCyan state = snapshot.data as ColorProjectStateCyan;    /// якщо ColorProjectState state дорівнює состоянию (snapshot.data) ColorProjectState   то ...
                    return Container(
                      width: 200,
                      height: 200,
                      color: state.isCyan ? Colors.greenAccent : Colors.lightBlueAccent,
                    );
                  }

                  return Container(        /// визначаємо вигляд віджета по замовчуванню до включення Bloc
                    width: 100,
                    height: 100,
                    color: Colors.deepPurple,
                  );

                })






          ],
        ),
      ),

      floatingActionButton: Row(
        mainAxisAlignment: MainAxisAlignment.end,
        children: <Widget>[

       FloatingActionButton(
      backgroundColor: Colors.cyanAccent,
        onPressed: () {
          bloc.event1();    /// метод натискання описано в  файлі bloc
        },
      ),

          SizedBox(width: 10,),

      FloatingActionButton(
        backgroundColor: Colors.red,
        onPressed: () {
          bloc.event();    /// метод натискання описано в  файлі bloc
        },
      ),
      ],),

    );
  }
}
